export default async function getItemsBoundingBox(items) {
  const boundingBox = {
    min: { x: Number.MAX_SAFE_INTEGER, y: Number.MAX_SAFE_INTEGER },
    max: { x: Number.MIN_SAFE_INTEGER, y: Number.MIN_SAFE_INTEGER },
  };

  items.forEach(({ boundingBox: { min, max } }) => {
    boundingBox.min.x = Math.min(boundingBox.min.x, min.x);
    boundingBox.min.y = Math.min(boundingBox.min.y, min.y);
    boundingBox.max.x = Math.max(boundingBox.max.x, max.x);
    boundingBox.max.y = Math.max(boundingBox.max.y, max.y);
  });

  boundingBox.width = boundingBox.max.x - boundingBox.min.x;
  boundingBox.height = boundingBox.max.y - boundingBox.min.y;
  boundingBox.center = {
    x: boundingBox.min.x + boundingBox.width / 2,
    y: boundingBox.min.y + boundingBox.height / 2,
  };

  return Promise.resolve(boundingBox);
}

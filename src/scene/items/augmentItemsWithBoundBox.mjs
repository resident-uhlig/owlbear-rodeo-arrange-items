import getItemBoundingBox from "./getItemBoundingBox.mjs";

export default async function augmentItemsWithBoundBox(items) {
  return Promise.all(
    items.map((item) =>
      getItemBoundingBox(item).then((boundingBox) => ({
        ...item,
        boundingBox,
      })),
    ),
  );
}

import deg2rad from "../../math/deg2rad.mjs";
import OBR, { Math2 } from "@owlbear-rodeo/sdk";
import rotate from "../../math/rotate.mjs";
import scale from "../../math/scale.mjs";

export default async function getItemBoundingBox(item) {
  switch (item.type) {
    case "CURVE":
      return getCurveBoundingBox(item);

    case "LINE":
      return getLineBoundingBox(item);

    case "PATH":
      break;

    case "TEXT":
      return getTextBoundingBox(item);

    case "IMAGE":
      return getImageBoundingBox(item);

    case "SHAPE":
      return getShapeBoundingBox(item);
  }

  return Promise.reject(`item.type unsupported: ${item.type}`);
}

async function getCurveBoundingBox(curve) {
  const boundingBox = Math2.boundingBox(curve.points);

  const radians = deg2rad(curve.rotation);
  const sin = Math.sin(radians);
  const cos = Math.cos(radians);

  const dx = -boundingBox.center.x * curve.scale.x;
  const dy = -boundingBox.center.y * curve.scale.y;

  const originalPosition = {
    x: boundingBox.center.x + dx * cos - dy * sin,
    y: boundingBox.center.y + dy * cos + dx * sin,
  };

  const offset = {
    x: curve.position.x - originalPosition.x,
    y: curve.position.y - originalPosition.y,
  };

  const points = curve.points.map((point) => {
    const scaled = scale(point, boundingBox.center, curve.scale);
    const rotated = rotate(scaled, boundingBox.center, curve.rotation);
    return {
      x: rotated.x + offset.x,
      y: rotated.y + offset.y,
    };
  });

  return Promise.resolve(Math2.boundingBox(points));
}

async function getLineBoundingBox(line) {
  const points = [
    {
      x: line.position.x + line.startPosition.x,
      y: line.position.y + line.startPosition.y,
    },
    {
      x: line.position.x + line.endPosition.x,
      y: line.position.y + line.endPosition.y,
    },
  ];

  return Promise.resolve(Math2.boundingBox(points));
}

async function getTextBoundingBox(text) {
  return Promise.resolve(OBR.scene.items.getItemBounds([text.id]));
}

async function getImageBoundingBox(image) {
  const scaleDpi = 150 / image.grid.dpi;
  const scaleX = scaleDpi * image.scale.x;
  const scaleY = scaleDpi * image.scale.y;

  const offsetX = image.grid.offset.x * scaleX;
  const offsetY = image.grid.offset.y * scaleY;

  const radians = deg2rad(image.rotation);
  const sin = Math.sin(radians);
  const cos = Math.cos(radians);

  return getRectangleBoundingBox({
    rotation: image.rotation,
    width: image.image.width,
    height: image.image.height,
    scale: {
      x: scaleX,
      y: scaleY,
    },
    position: {
      x: image.position.x - offsetX * cos + offsetY * sin,
      y: image.position.y - offsetY * cos - offsetX * sin,
    },
  });
}

async function getShapeBoundingBox(shape) {
  switch (shape.shapeType) {
    case "CIRCLE":
      return getCircleBoundingBox(shape);

    case "HEXAGON":
      return getHexagonBoundingBox(shape);

    case "RECTANGLE":
      return getRectangleBoundingBox(shape);

    case "TRIANGLE":
      return getTriangleBoundingBox(shape);

    default:
      return Promise.reject(`shape.shapeType unsupported: ${shape.shapeType}`);
  }
}

async function getCircleBoundingBox(circle) {
  const width = circle.width * circle.scale.x;
  const height = circle.height * circle.scale.y;
  const radius = Math.min(width, height) / 2;

  return Promise.resolve({
    min: { x: circle.position.x - radius, y: circle.position.y - radius },
    max: { x: circle.position.x + radius, y: circle.position.y + radius },
    center: { x: circle.position.x, y: circle.position.y },
    width,
    height,
  });
}

async function getHexagonBoundingBox(hexagon) {
  const radius = (hexagon.width / 2) * hexagon.scale.x;
  const radians = deg2rad(hexagon.rotation);
  const corners = [
    Math.PI / 2,
    Math.PI / 6,
    (11 * Math.PI) / 6,
    (3 * Math.PI) / 2,
    (7 * Math.PI) / 6,
    (5 * Math.PI) / 6,
  ].map((angle) => {
    angle = angle + radians;
    return {
      x: hexagon.position.x + radius * Math.cos(angle),
      y: hexagon.position.y + radius * Math.sin(angle),
    };
  });

  return Promise.resolve(Math2.boundingBox(corners));
}

async function getRectangleBoundingBox(rectangle) {
  const radians = deg2rad(rectangle.rotation);
  const sin = Math.sin(radians);
  const cos = Math.cos(radians);

  const rX = (rectangle.width / 2) * rectangle.scale.x;
  const rY = (rectangle.height / 2) * rectangle.scale.y;

  const center = {
    x: rectangle.position.x + rX * cos - rY * sin,
    y: rectangle.position.y + rY * cos + rX * sin,
  };

  const corners = [
    {
      x: center.x - rX * cos + rY * sin,
      y: center.y - rX * sin - rY * cos,
    },
    {
      x: center.x + rX * cos + rY * sin,
      y: center.y + rX * sin - rY * cos,
    },
    {
      x: center.x + rX * cos - rY * sin,
      y: center.y + rX * sin + rY * cos,
    },
    {
      x: center.x - rX * cos - rY * sin,
      y: center.y - rX * sin + rY * cos,
    },
  ];

  return Promise.resolve(Math2.boundingBox(corners));
}

async function getTriangleBoundingBox(triangle) {
  const radians = deg2rad(triangle.rotation);
  const sin = Math.sin(radians);
  const cos = Math.cos(radians);

  const halfWidth = (triangle.width / 2) * triangle.scale.x;
  const height = triangle.height * triangle.scale.y;

  const corners = [
    {
      x: triangle.position.x,
      y: triangle.position.y,
    },
    {
      x: triangle.position.x - halfWidth * cos - height * sin,
      y: triangle.position.y + height * cos - halfWidth * sin,
    },
    {
      x: triangle.position.x + halfWidth * cos - height * sin,
      y: triangle.position.y + height * cos + halfWidth * sin,
    },
  ];

  return Promise.resolve(Math2.boundingBox(corners));
}

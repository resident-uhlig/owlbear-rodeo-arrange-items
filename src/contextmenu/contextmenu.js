"use strict";

import OBR from "@owlbear-rodeo/sdk";
import getText from "../i18n/getText.mjs";
import en_US from "../i18n/locales/en_US.mjs";
import augmentItemsWithBoundBox from "../scene/items/augmentItemsWithBoundBox.mjs";
import arrangeItemsHorizontally from "../arrange/arrangeItemsHorizontally.mjs";
import arrangeItemsInCircle from "../arrange/arrangeItemsInCircle.mjs";
import arrangeItemsVertically from "../arrange/arrangeItemsVertically.mjs";
import arrangeItemsInSquare from "../arrange/arrangeItemsInSquare.mjs";
import arrangeItemsRandomly from "../arrange/arrangeItemsRandomly.mjs";
import getItemsBoundingBox from "../scene/items/getItemsBoundingBox.mjs";

function showError(error) {
  return OBR.notification.show("" + error, "ERROR");
}

OBR.onReady(() => {
  Promise.all([fetch("meta.json").then((response) => response.json())]).then(
    ([{ id, basePath }]) => {
      const locale = en_US;

      [
        {
          idKey: "horizontal",
          icon: "grip-solid",
          i18nKey: "horizontal-line",
          onClick(context) {
            augmentItemsWithBoundBox(context.items)
              .then((items) =>
                getItemsBoundingBox(items).then((selectionBounds) =>
                  arrangeItemsHorizontally(selectionBounds, items),
                ),
              )
              .catch(showError);
          },
        },
        {
          idKey: "vertical",
          icon: "grip-vertical-solid",
          i18nKey: "vertical-line",
          onClick(context) {
            augmentItemsWithBoundBox(context.items)
              .then((items) =>
                getItemsBoundingBox(items).then((selectionBounds) =>
                  arrangeItemsVertically(selectionBounds, items),
                ),
              )
              .catch(showError);
          },
        },
        {
          idKey: "circle",
          icon: "circle-regular",
          i18nKey: "circle",
          onClick(context) {
            augmentItemsWithBoundBox(context.items)
              .then((items) =>
                getItemsBoundingBox(items).then((selectionBounds) =>
                  arrangeItemsInCircle(selectionBounds, items),
                ),
              )
              .catch(showError);
          },
        },
        {
          idKey: "square",
          icon: "square-regular",
          i18nKey: "square",
          onClick(context) {
            augmentItemsWithBoundBox(context.items)
              .then((items) =>
                getItemsBoundingBox(items).then((selectionBounds) =>
                  arrangeItemsInSquare(selectionBounds, items),
                ),
              )
              .catch(showError);
          },
        },
        {
          idKey: "random",
          icon: "dice-solid",
          i18nKey: "random",
          onClick(context) {
            augmentItemsWithBoundBox(context.items)
              .then((items) =>
                getItemsBoundingBox(items).then((selectionBounds) =>
                  arrangeItemsRandomly(selectionBounds, items),
                ),
              )
              .catch(showError);
          },
        },
      ].forEach(({ idKey, icon, i18nKey, onClick }) => {
        OBR.contextMenu.create({
          id: `${id}/${idKey}`,
          icons: [
            {
              icon: `${basePath}font-awesome/svgs/${icon}.svg`,
              label: getText(
                locale,
                "context_menu.arrange_items",
                getText(locale, `formation.${i18nKey}`),
              ),
              filter: {
                min: 2,
                permissions: ["UPDATE"],
              },
            },
          ],
          onClick,
        });
      });
    },
  );
});

const en_US = {
  locale: "en-US",
  name: "English (US)",

  "context_menu.arrange_items": (formation) => `Arrange Items: ${formation}`,

  "formation.horizontal-line": "Horizontal Line",
  "formation.vertical-line": "Vertical Line",
  "formation.circle": "Circle",
  "formation.square": "Square",
  "formation.random": "Random",
};

export default en_US;

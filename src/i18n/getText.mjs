const unknownIds = [];

export default function getText(locale, id, ...parameters) {
  const text = locale[id];
  if (text === undefined) {
    if (!unknownIds.includes(id)) {
      unknownIds.push(id);
      console.warn(`Unknown id: ${id}`);
    }

    return id;
  }

  if (typeof text === "function") {
    return text(...parameters);
  }

  return text;
}

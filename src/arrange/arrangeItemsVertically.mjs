import OBR from "@owlbear-rodeo/sdk";

export default async function arrangeItemsVertically(selectionBounds, items) {
  items = [...items];
  items.sort((a, b) => {
    const delta = a.boundingBox.min.y - b.boundingBox.min.y;
    if (delta === 0) {
      return a.boundingBox.min.x - b.boundingBox.min.x;
    }

    return delta;
  });

  const totalHeight = selectionBounds.height;
  const totalItemsHeight = items.reduce(
    (total, { boundingBox: { height } }) => total + height,
    0,
  );

  if (totalItemsHeight < totalHeight) {
    const totalSpaceHeight = totalHeight - totalItemsHeight;
    const spaceHeight = totalSpaceHeight / (items.length - 1);
    return arrangeItems(
      items,
      {
        x: selectionBounds.center.x,
        y: selectionBounds.min.y,
      },
      spaceHeight,
    );
  }

  return arrangeItems(
    items,
    {
      x: selectionBounds.center.x,
      y: selectionBounds.center.y - totalItemsHeight / 2,
    },
    0,
  );
}

async function arrangeItems(items, { x, y }, spaceHeight) {
  return OBR.scene.items.updateItems(items, (draft) => {
    draft.forEach((item, index) => {
      const { boundingBox } = items[index];
      item.position.x =
        x - boundingBox.width / 2 + item.position.x - boundingBox.min.x;
      item.position.y = y + item.position.y - boundingBox.min.y;
      y += boundingBox.height + spaceHeight;
    });
  });
}

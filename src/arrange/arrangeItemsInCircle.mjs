import OBR from "@owlbear-rodeo/sdk";

export default function arrangeItemsInCircle(selectionBounds, items) {
  const radius = Math.min(selectionBounds.width, selectionBounds.height) / 2;
  return OBR.scene.items.updateItems(items, (draft) => {
    draft.forEach((item, index) => {
      const angle = (2 * Math.PI * index) / items.length;
      const { boundingBox } = items[index];
      const x = selectionBounds.center.x + radius * Math.cos(angle);
      const y = selectionBounds.center.y + radius * Math.sin(angle);

      item.position.x = x + item.position.x - boundingBox.min.x;
      item.position.y = y + item.position.y - boundingBox.min.y;
    });
  });
}

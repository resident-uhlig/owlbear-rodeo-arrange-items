import OBR from "@owlbear-rodeo/sdk";

export default async function arrangeItemsHorizontally(selectionBounds, items) {
  items = [...items];
  items.sort((a, b) => {
    const delta = a.boundingBox.min.x - b.boundingBox.min.x;
    if (delta === 0) {
      return a.boundingBox.min.y - b.boundingBox.min.y;
    }

    return delta;
  });

  const totalWidth = selectionBounds.width;
  const totalItemsWidth = items.reduce(
    (total, { boundingBox: { width } }) => total + width,
    0,
  );

  if (totalItemsWidth < totalWidth) {
    const totalSpaceWidth = totalWidth - totalItemsWidth;
    const spaceWidth = totalSpaceWidth / (items.length - 1);
    return arrangeItems(
      items,
      {
        x: selectionBounds.min.x,
        y: selectionBounds.center.y,
      },
      spaceWidth,
    );
  }

  return arrangeItems(
    items,
    {
      x: selectionBounds.center.x - totalItemsWidth / 2,
      y: selectionBounds.center.y,
    },
    0,
  );
}

async function arrangeItems(items, { x, y }, spaceWidth) {
  return OBR.scene.items.updateItems(items, (draft) => {
    draft.forEach((item, index) => {
      const { boundingBox } = items[index];
      item.position.x = x + item.position.x - boundingBox.min.x;
      item.position.y =
        y - boundingBox.height / 2 + item.position.y - boundingBox.min.y;
      x += boundingBox.width + spaceWidth;
    });
  });
}

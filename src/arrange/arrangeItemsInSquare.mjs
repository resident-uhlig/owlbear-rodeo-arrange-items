import OBR from "@owlbear-rodeo/sdk";

export default function arrangeItemsInSquare(selectionBounds, items) {
  const { max } = calculateStatistics(items);

  const sqrt = Math.sqrt(items.length);
  const columns = Math.round(sqrt);
  const totalWidth = selectionBounds.width;
  const totalItemsWidth = columns * max.width;
  let spaceWidth, startX;
  if (totalItemsWidth < totalWidth) {
    const totalSpaceWidth = totalWidth - totalItemsWidth;
    spaceWidth = totalSpaceWidth / (columns - 1);
    startX = selectionBounds.min.x;
  } else {
    spaceWidth = 0;
    startX = selectionBounds.center.x - totalItemsWidth / 2;
  }

  const rows = columns;
  const totalHeight = selectionBounds.height;
  const totalItemsHeight = rows * max.height;
  let spaceHeight, startY;
  if (totalItemsHeight < totalHeight) {
    const totalSpaceHeight = totalHeight - totalItemsHeight;
    spaceHeight = totalSpaceHeight / (rows - 1);
    startY = selectionBounds.min.y;
  } else {
    spaceHeight = 0;
    startY = selectionBounds.center.y - totalItemsHeight / 2;
  }

  return OBR.scene.items.updateItems(items, (draft) => {
    let x = startX;
    let y = startY;
    draft.forEach((item, index) => {
      const { boundingBox } = items[index];
      item.position.x =
        x +
        (max.width - boundingBox.width) / 2 +
        item.position.x -
        boundingBox.min.x;
      item.position.y =
        y +
        (max.height - boundingBox.height) / 2 +
        item.position.y -
        boundingBox.min.y;

      x += max.width + spaceWidth;
      if (index > 0 && (index + 1) % columns === 0) {
        x = startX;
        y += max.height + spaceHeight;
      }
    });
  });
}

function calculateStatistics(items) {
  return items.reduce(
    (statistics, { boundingBox: { width, height } }) => {
      if (statistics.max.width === undefined || width > statistics.max.width) {
        statistics.max.width = width;
      }

      if (
        statistics.max.height === undefined ||
        height > statistics.max.height
      ) {
        statistics.max.height = height;
      }

      return statistics;
    },
    { max: {} },
  );
}

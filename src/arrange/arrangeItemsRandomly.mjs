import OBR, { buildShape } from "@owlbear-rodeo/sdk";

export default function arrangeItemsRandomly(selectionBounds, items) {
  // items.forEach(({ boundingBox }) => {
  //   const shape = buildShape()
  //     .fillOpacity(0)
  //     .width(boundingBox.width)
  //     .height(boundingBox.height)
  //     .position({
  //       x: boundingBox.min.x,
  //       y: boundingBox.min.y
  //     })
  //     .build();
  //
  //   OBR.scene.local.addItems([shape]);
  //   setTimeout(() => OBR.scene.local.deleteItems([shape.id]), 5000);
  // });

  return OBR.scene.items.updateItems(items, (draft) => {
    draft.forEach((item, index) => {
      const { boundingBox } = items[index];
      item.position.x =
        selectionBounds.min.x +
        Math.random() * (selectionBounds.width - boundingBox.width);

      item.position.y =
        selectionBounds.min.y +
        Math.random() * (selectionBounds.height - boundingBox.height);
    });
  });
}

import deg2rad from "./deg2rad.mjs";

export default function rotate(point, center, degree) {
  const radians = deg2rad(degree);
  const sin = Math.sin(radians);
  const cos = Math.cos(radians);

  const dx = point.x - center.x;
  const dy = point.y - center.y;

  return {
    x: dx * cos - dy * sin + center.x,
    y: dy * cos + dx * sin + center.y,
  };
}

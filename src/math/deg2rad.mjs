export default function deg2rad(degree) {
  return (degree / 180) * Math.PI;
}

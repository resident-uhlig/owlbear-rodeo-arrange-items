export default function scale(point, center, scale) {
  return {
    x: (point.x - center.x) * scale.x + center.x,
    y: (point.y - center.y) * scale.y + center.y,
  };
}

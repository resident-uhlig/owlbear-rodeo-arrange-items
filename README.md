# Arrange Items Extension for Owlbear Rodeo

<a href="https://gitlab.com/resident-uhlig/legacy-code"><img alt="Legacy Code: O++ S++ I C-- E- M+ V+ D+ !A" src="https://img.shields.io/badge/Legacy%20Code-O%2B%2B%20S%2B%2B%20I%20C----%20E--%20M%2B%20V%2B%20D%2B%20!A-informational"></a>

## About

This extension helps you to arrange items in a scene for [Owlbear Rodeo].

![Animated GIF of the extension in action](static/store/arrange-items.gif)

[Owlbear Rodeo]: https://owlbear.rodeo/

## Installation, Features, Usage, Support

Please go to the [store page] for details.

[store page]: https://owlbear.rogue.pub/extension/https%3A%2F%2Fresident-uhlig.gitlab.io%2Fowlbear-rodeo-arrange-items%2Fmanifest.json

## Dependencies

At runtime, this extension depends on:

- [Owlbear Rodeo SDK]

The rest is written in vanilla JavaScript.

However, there are some dependencies for developing purpose,
that are managed using [npm] in [package.json](package.json).

[Owlbear Rodeo SDK]: https://github.com/owlbear-rodeo/sdk
[npm]: https://www.npmjs.com/

## Development

### Contribute

If you want to contribute, you can do this by [creating an issue]. Because my
time is limited, please be patient and if you want to submit some code, please
wait for my response before you start any actual coding.

[creating an issue]: https://gitlab.com/resident-uhlig/owlbear-rodeo-arrange-items/-/issues/new

### Prepare the local source code

1. `git clone` the repository.
2. Run `npm install` in the repository root.

### Use local version in Owlbear Rodeo

1. `npm start` in the repository root.
2. [Install Your Extension] using URL <https://localhost:8080/owlbear-rodeo-arrange-items/manifest.json>.

[Install Your Extension]: https://docs.owlbear.rodeo/extensions/tutorial-hello-world/install-your-extension/

If this does not work, then it is most likely because of issues with the
self-signed certificate for localhost. Check your browser's JavaScript console.

Work-around for Chrome-based browsers:

1. Open the given URL in your browser.
2. Type `thisisunsafe` [^thisisunsafe]

[^thisisunsafe]: https://gist.github.com/RobertKeyser/e1dd0d6ff814c120c0b84575d266d9f7 "GitHub gist: Bypass Cert Errors in Chromium-based Browsers"

### Build production version

1. Run `npm run build` in the repository root.

### Backlog

#### Prioritized

#### To be prioritized

- docs: create gif of how to arrange items
- build: create hero image using cypress
- build: create gif of how to arrange items

### Update dependencies

1. `ncu` checks the dependencies.
2. `ncu -u` actually updates the dependencies.
3. `npm install` installs the updated dependencies.

### Code style

Use [.editorconfig](.editorconfig) for pre-formatting the source code in your
IDE. Enable git hooks to automatically format the code according
to [.prettierrc](.prettierrc) while commiting changes.

## License

### For this software

> Copyright (c) 2024 Sven Uhlig <git@resident-uhlig.de>
>
> Permission is hereby granted, free of charge, to any person obtaining a copy
> of this software and associated documentation files (the "Software"), to deal
> in the Software without restriction, including without limitation the rights
> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
> copies of the Software, and to permit persons to whom the Software is
> furnished to do so, subject to the following conditions:
>
> The above copyright notice and this permission notice shall be included in all
> copies or substantial portions of the Software.
>
> The Software shall be used for Good and must NOT be used for Evil.
>
> The Software must NOT be used to operate nuclear facilities, weapons, things
> owned by a state or its contractors, life support or mission-critical
> applications where human life or property may be at stake.
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
> SOFTWARE.

[Impressum](https://resident-uhlig.de/impressum.html)

### For 3rd parties

- [Font Awesome Free License](static/font-awesome/LICENSE.txt)
- [Owlbear Rodeo SDK License](https://github.com/owlbear-rodeo/sdk/blob/main/LICENSE)

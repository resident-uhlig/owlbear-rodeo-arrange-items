const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");
const meta = require("./package.json");
const CaseSensitivePathsPlugin = require("@umijs/case-sensitive-paths-webpack-plugin");

module.exports = (env, argv) => ({
  mode: "development",
  entry: {
    contextmenu: "./src/contextmenu/contextmenu.js",
  },
  devServer: {
    server: "https",
    devMiddleware: {
      publicPath: "/owlbear-rodeo-arrange-items",
    },
    headers: {
      "Access-Control-Allow-Origin": "*",
    },
  },
  plugins: [
    new CaseSensitivePathsPlugin(),
    new HtmlWebpackPlugin({
      title: meta.name,
      filename: `[name].html`,
      favicon: "static/font-awesome/svgs/grip-solid.svg",
    }),
    new CopyPlugin({
      patterns: [
        {
          from: "**/*",
          context: "static/",
          globOptions: {
            ignore: ["**/*.{json,md}"],
          },
        },
        {
          from: "**/*.{json,md}",
          context: "static/",
          transform(content) {
            let name = meta.name;
            let version = meta.version;
            if (argv.mode !== "production") {
              name += " (Development)";
              version += "-dev";
            }

            return content
              .toString()
              .replaceAll("$VERSION$", version)
              .replaceAll("$BUILD_DATE_TIME$", new Date().toISOString())
              .replaceAll("$NAME$", name)
              .replaceAll("$DESCRIPTION$", meta.description)
              .replaceAll("$AUTHOR$", meta.author.name)
              .replaceAll("$HOMEPAGE$", meta.homepage)
              .replaceAll("$BASE_PATH$", "/owlbear-rodeo-arrange-items/")
              .replaceAll(
                "$GITLAB_PAGES$",
                "https://resident-uhlig.gitlab.io/owlbear-rodeo-arrange-items/",
              );
          },
        },
      ],
    }),
  ],
  output: {
    filename: "[name].[contenthash].js",
    path: path.resolve(__dirname, "public"),
    clean: true,
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.m?jsx?$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: [["@babel/preset-react", { runtime: "automatic" }]],
          },
        },
      },
    ],
  },
});

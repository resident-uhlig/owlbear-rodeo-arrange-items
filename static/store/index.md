---
title: $NAME$
description: $DESCRIPTION$
author: $AUTHOR$
image: $GITLAB_PAGES$store/arrange-items.gif
icon: $GITLAB_PAGES$font-awesome/svgs/grip-solid.svg
tags:
  - tool
manifest: $GITLAB_PAGES$manifest.json
learn-more: $HOMEPAGE$
---

# $NAME$

## About

This extension helps you to arrange items in a scene.

## Features

- Arrange selected items in a horizontal line
- Arrange selected items in a vertical line
- Arrange selected items in a circle
- Arrange selected items in a square

## Installation

[Install Your Extension] using the URL [$GITLAB_PAGES$manifest.json](../manifest.json).

[Install Your Extension]: https://docs.owlbear.rodeo/extensions/tutorial-hello-world/install-your-extension/

## Usage

### Permissions

Only players with the "UPDATE" permission can use this extension.

### Arrange Items

1. Select the items that you want to arrange (at least 2)
2. Open the context menu
3. Click on "Arrange Items" to arrange items in a horizontal line
